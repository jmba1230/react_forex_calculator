import React, { Component } from 'react';
// import Box Component
import Box from './components/Box';
// import Buttons Component
import Buttons from './components/Buttons';

class App extends Component {

  state = {
    count: 0
  }

  handleAdd = () => {
    this.setState({
      count: this.state.count + 1
    });
  }

  handleMinus = () => {
    this.setState({
      count: this.state.count - 1
    });
  }

  handleReset = () => {
    this.setState({
      count: 0
    });
  }

  handleMultiply = () => {
    this.setState({
      count: this.state.count * 2
    });
  }

  // Activity, set up the functions of the remaining buttons.

  render() {
    return (
      <>
        <Box
          count={this.state.count}
        />
        <Buttons
          // Step 2: Pass the functions using Props to Button Component
          handleAdd={this.handleAdd}
          handleMinus={this.handleMinus}
          handleReset={this.handleReset}
          handleMultiply={this.handleMultiply}
        />
      </>
    )
  }
}

export default App;