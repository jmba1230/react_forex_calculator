import React, { useState } from 'react';
import ForexDropdown from './ForexDropdown';
import ForexInput from './ForexInput';
import { Button } from 'reactstrap';
import ForexTable from './ForexTable';

const Forex = () => {

    // Transform this to functional syntax
    const [amount, setAmount] = useState(0);
    const [baseCurrency, setBaseCurrency] = useState('');
    const [targetCurrency, setTargetCurrency] = useState('');
    const [convertedAmount, setConvertedAmount] = useState(0);
    const [showTable, setShowTable] = useState(false);
    const [rates, setRates] = useState([]);

    const handleAmount = e => {
        // this.setState({
        //     amount: e.target.value
        // });
        setAmount(e.target.value);
    }

    const handleBaseCurrency = currency => {
        // this.setState({
        //     baseCurrency: currency
        // })
        const code = currency.code;

        fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then(res => res.json())
            .then(res => {

                const ratesArray = Object.entries(res.rates);

                setRates(ratesArray);
                setShowTable(true);
            });

        setBaseCurrency(currency);
    }

    const handleTargetCurrency = currency => {
        // this.setState({
        //     targetCurrency: currency
        // });
        setTargetCurrency(currency);
    }

    const handleConvert = () => {

        if (baseCurrency === "" || targetCurrency === "") {
            alert('No Base or Target Currency selected');
        } else if (amount <= 0) {
            alert('Invalid Amount');
        } else {
            const code = baseCurrency.code;



            fetch('https://api.exchangeratesapi.io/latest?base=' + code)
                .then(res => res.json())
                .then(res => {
                    const targetCode = targetCurrency.code;

                    const rate = res.rates[targetCode];

                    // // console.log(rate);
                    // this.setState({ convertedAmount: this.state.amount * rate })
                    setConvertedAmount(amount * rate)
                });
        }
    }

    // remove all this and state
    return (
        <div
            style={{ width: '70%' }}
        >
            <h1 className='text-center my-5'>Forex Calculator</h1>
            {
                showTable === true
                    ?
                    <div>
                        <h2 className='text-center'>Exchange Rate for: {baseCurrency.currency}</h2>
                        <div className='d-flex justify-content-center'>
                            <ForexTable
                                rates={rates}
                            />
                        </div>
                    </div>
                    : ''
            }
            <div
                className='d-flex justify-content-around'
                style={{ margin: '0 200px' }}
            >

                <ForexDropdown
                    label={'Base Currency'}
                    onClick={handleBaseCurrency}
                    currency={baseCurrency}
                />
                <ForexDropdown
                    label={'Target Currency'}
                    onClick={handleTargetCurrency}
                    currency={targetCurrency}
                />
            </div>
            <div
                className='d-flex justify-content-around'
            >
                <ForexInput
                    label={'Amount'}
                    placeholder={'Amount to convert'}
                    onChange={handleAmount}
                />
                <Button
                    color='info'
                    onClick={handleConvert}
                >
                    Convert
                    </Button>
            </div>
            <div>
                <h1 className='text-center'>{convertedAmount}</h1>
            </div>
        </div>
    )
}

export default Forex;