import React from 'react';
import { Table } from 'reactstrap';

const ForexTable = props => {
    return (
        <Table className='striped border'>
            <thead>
                <tr>
                    <th>Currency</th>
                    <th>Rate</th>
                </tr>
            </thead>
            <tbody>
                {props.rates.map((rate, index) => (
                    <tr
                        key={index}
                    >
                        <td>{rate[0]}</td>
                        <td>{rate[1]}</td>
                    </tr>
                ))}
            </tbody>
        </Table>
    )
}

export default ForexTable